package com.sb.socialbook.service.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sb.socialbook.model.CategoryModel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookDTO {

	@JsonProperty("_isbn")
	private int isbn;

	@JsonProperty("_title")
	private String name;

	@JsonProperty("_description")
	private String description;

	@JsonProperty("_language")
	private String language;

	@JsonProperty("_frontCoverLink")
	private String pictureIdFrontCover;

	@JsonProperty("_categories")
	private List<CategoryModel> categories;

	@JsonProperty("_authors")
	private List<AuthorDTO> authors;
}