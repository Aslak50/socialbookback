package com.sb.socialbook.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 *
 * Représente l'objet reader récupéré du front.
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReaderDTO {
	@JsonProperty("_firstName")
	private String firstname;

	@JsonProperty("_lastName")
	private String lastname;

	@JsonProperty("_location")
	private String location;

	@JsonProperty("_mail")
	private String mail;

	@JsonProperty("_password")
	private String password;

	@JsonProperty("_phoneNumber")
	private String phoneNumber;
}
