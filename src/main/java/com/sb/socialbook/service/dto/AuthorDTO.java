package com.sb.socialbook.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.ToString;
/**
 *
 * Représente l'objet auteur récupéré du front.
 *
 */
@ToString
public class AuthorDTO {

	@JsonProperty("_nom")
	private String nom;

	public AuthorDTO() {
	}

	public AuthorDTO(String nom) {
		this.nom = nom;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}
