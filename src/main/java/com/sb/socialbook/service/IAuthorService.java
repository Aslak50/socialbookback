package com.sb.socialbook.service;

import com.sb.socialbook.service.dto.AuthorDTO;

/**
 *
 * Permet de transférer les objets auteurDTO à la couche persistance repository
 * via le modèle AuthorModel.
 *
 */
public interface IAuthorService {
	/**
	 * Envoie les informations des auteurs vers la couche persistance.
	 *
	 * @param author
	 * @return AuthorDTO
	 */
	public AuthorDTO addAuthors(AuthorDTO author);

	// supprimer un auteur
}