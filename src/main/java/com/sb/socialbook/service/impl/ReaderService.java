package com.sb.socialbook.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sb.socialbook.model.ReaderModel;
import com.sb.socialbook.repository.IReaderRepository;
import com.sb.socialbook.service.dto.ReaderDTO;

@Service
@Transactional
public class ReaderService {

	private IReaderRepository iReaderRepository;

	@Autowired
	public ReaderService(IReaderRepository iReaderRepository) {
		this.iReaderRepository = iReaderRepository;
	}

	public ReaderDTO registerReader(ReaderDTO readerDTO) {
		// Transmet les données du readerDTO au readerModel
		ReaderModel readerModel = new ReaderModel(readerDTO.getFirstname(), readerDTO.getLastname(),
				readerDTO.getLocation(), readerDTO.getMail(), readerDTO.getPassword(), readerDTO.getPhoneNumber());

		// appelle la méthode save du readerRepository
		this.iReaderRepository.save(readerModel);

		return readerDTO;
	}

}
