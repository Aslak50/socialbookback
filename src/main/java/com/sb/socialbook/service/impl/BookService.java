package com.sb.socialbook.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sb.socialbook.model.AuthorModel;
import com.sb.socialbook.model.BookModel;
import com.sb.socialbook.repository.BookRepository;
import com.sb.socialbook.repository.IAuthorRepository;
import com.sb.socialbook.repository.IBookRepository;
import com.sb.socialbook.repository.ICategoryRepository;
import com.sb.socialbook.service.IBookService;
import com.sb.socialbook.service.dto.AuthorDTO;
import com.sb.socialbook.service.dto.BookDTO;

@Service
@Transactional
public class BookService implements IBookService {

	BookRepository bookRepository;
	IBookRepository iBookRepository;
	IAuthorRepository iAuthorRepository;
	ICategoryRepository iCategoryRepository;

	@Autowired
	public BookService(BookRepository bookRepository, IBookRepository iBookRepository,
			IAuthorRepository iAuthorRepository, ICategoryRepository iCategoryRepository) {
		this.bookRepository = bookRepository;
		this.iBookRepository = iBookRepository;
		this.iAuthorRepository = iAuthorRepository;
		this.iCategoryRepository = iCategoryRepository;

	}

	@Override
	public BookDTO addBook(BookDTO bookDTO) {
		// BookModel bookModel = new BookModel(bookDTO.getIsbn(), bookDTO.getName(),
		// bookDTO.getDescription());
		// this.bookRepository.save(bookModel);
		return bookDTO;
	}

	public BookModel find(Integer id) {
		BookModel book = this.iBookRepository.findById(id).get();
		return book;
	}

	public BookModel save(Integer id) {
		BookModel b = this.iBookRepository.findById(1234).get();
		AuthorModel a = this.iAuthorRepository.findById(29).get();
		b.getAuthors().add(a);
		a.getBooks().add(b);
		BookModel savedB = this.iBookRepository.save(b);
		System.out.println(savedB);
		return savedB;
	}

	/**
	 * Enregistre un Livre en BDD avec son auteur et sa catégorie
	 * @param bookModel
	 * @param authorModel
	 * @param categoryModel
	 * @return
	 */
	public BookDTO saveTest(BookDTO bookDTO) {
		BookModel bookModel = new BookModel();

		// convertit le bookDTO en bookModèle pour enregistrement en BDD
		bookModel.setIsbn(bookDTO.getIsbn());
		bookModel.setName(bookDTO.getName());
		bookModel.setDescription(bookDTO.getDescription());
		bookModel.setLanguage(bookDTO.getLanguage());
		bookModel.setPictureIdFrontCover(bookDTO.getPictureIdFrontCover());

		// récupère la liste des auteurs du bookDTO envoyé
		List<AuthorDTO> listAuthorDTO = bookDTO.getAuthors();

		// pour chaque auteur du livre :
		for (AuthorDTO authorDTO : listAuthorDTO) {
			//créer un auteurModèle avec le nom du DTO
			AuthorModel am = new AuthorModel();
			am.setName(authorDTO.getNom());

			//L'ajouter à la liste des auteurs du bookModèle
			bookModel.getAuthors().add(am);
			am.getBooks().add(bookModel);

			// enregistrer le ou les auteursModèle en BDD
			this.iAuthorRepository.save(am);
		}

		// Enregistrer le bookModèle en BDD
		this.iBookRepository.save(bookModel);

		System.out.println(bookModel);

		return bookDTO;
	}

	/**
	 * Trouver un livre par auteur
	 * @param authorName
	 * @return une liste de Livres écrits par l'auteur spécifié
	 */
	public List<BookModel> findBookByAuthor(String authorName) {
		List<BookModel> listBook = null;
		listBook = this.iBookRepository.findByauthorsName(authorName);
		return listBook;
	}

	/**
	 * Trouver un livre par catégorie
	 * @param authorName
	 * @return une liste de Livres de la catégorie spécifiée
	 */
	public List<BookModel> findBookByCategory(String categoryName) {
		List<BookModel> listBook = null;
		listBook = this.iBookRepository.findBycategoriesName(categoryName);
		return listBook;
	}
}