package com.sb.socialbook.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sb.socialbook.repository.AuthorRepository;
import com.sb.socialbook.service.IAuthorService;
import com.sb.socialbook.service.dto.AuthorDTO;

@Service
public class AuthorService implements IAuthorService {

	@Autowired
	private AuthorRepository authorRepo;

	@Override
	public AuthorDTO addAuthors(AuthorDTO authorDTO) {
		//AuthorModel authorModel = new AuthorModel(authorDTO.getNom());
		//this.authorRepo.save(authorModel);
		return authorDTO;
	}

}
