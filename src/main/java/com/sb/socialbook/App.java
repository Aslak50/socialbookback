package com.sb.socialbook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.sb.socialbook.service.impl.BookService;

@SpringBootApplication
public class App {

	public static void main(String[] args) {
		ApplicationContext conteneur = SpringApplication.run(App.class, args);

		// appel des services
		BookService bss = conteneur.getBean(BookService.class);

		//		List<BookModel> BM = bss.findBookByAuthor("Aslak");
		//		BM.forEach(s->System.out.println(s.getAuthors().get(0).getName() +" a écrit : " + s.getName()));
		//
		//		List<BookModel> BM2 = bss.findBookByCategory("Aventure");
		//		BM2.forEach(s->System.out.println("Le livre \"" + s.getName() +"\" est de la catégorie : " + s.getCategories().get(0).getName()));
	}
}
