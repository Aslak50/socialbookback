package com.sb.socialbook.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the proposal database table.
 * 
 */
@Entity
@Table(name="proposal")
@NamedQuery(name="ProposalModel.findAll", query="SELECT p FROM ProposalModel p")
public class ProposalModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="created_at")
	private Timestamp createdAt;

	//bi-directional many-to-one association to MessageModel
	@OneToMany(mappedBy="proposal")
	private List<MessageModel> messages;

	//bi-directional many-to-one association to BookModel
	@ManyToOne
	@JoinColumn(name="book_receiving_isbn")
	private BookModel book1;

	//bi-directional many-to-one association to BookModel
	@ManyToOne
	@JoinColumn(name="book_porposing_isbn")
	private BookModel book2;

	//bi-directional many-to-one association to EtatModel
	@ManyToOne
	private EtatModel etat;

	//bi-directional many-to-one association to ReaderModel
	@ManyToOne
	@JoinColumn(name="reader_receiving_id")
	private ReaderModel reader1;

	//bi-directional many-to-one association to ReaderModel
	@ManyToOne
	@JoinColumn(name="reader_proposing_id")
	private ReaderModel reader2;

	public ProposalModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public List<MessageModel> getMessages() {
		return this.messages;
	}

	public void setMessages(List<MessageModel> messages) {
		this.messages = messages;
	}

	public MessageModel addMessage(MessageModel message) {
		getMessages().add(message);
		message.setProposal(this);

		return message;
	}

	public MessageModel removeMessage(MessageModel message) {
		getMessages().remove(message);
		message.setProposal(null);

		return message;
	}

	public BookModel getBook1() {
		return this.book1;
	}

	public void setBook1(BookModel book1) {
		this.book1 = book1;
	}

	public BookModel getBook2() {
		return this.book2;
	}

	public void setBook2(BookModel book2) {
		this.book2 = book2;
	}

	public EtatModel getEtat() {
		return this.etat;
	}

	public void setEtat(EtatModel etat) {
		this.etat = etat;
	}

	public ReaderModel getReader1() {
		return this.reader1;
	}

	public void setReader1(ReaderModel reader1) {
		this.reader1 = reader1;
	}

	public ReaderModel getReader2() {
		return this.reader2;
	}

	public void setReader2(ReaderModel reader2) {
		this.reader2 = reader2;
	}

}