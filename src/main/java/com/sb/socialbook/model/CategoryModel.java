package com.sb.socialbook.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.ToString;


/**
 * The persistent class for the category database table.
 *
 */
@Entity
@ToString
@Table(name="category")
@NamedQuery(name="CategoryModel.findAll", query="SELECT c FROM CategoryModel c")
public class CategoryModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String name;

	//bi-directional many-to-many association to BookModel
	@ToString.Exclude
	@ManyToMany
	@JoinTable(
			name="belong"
			, joinColumns={
					@JoinColumn(name="category_id")
			}
			, inverseJoinColumns={
					@JoinColumn(name="book_id")
			}
			)
	private List<BookModel> books = new ArrayList<BookModel>();

	public CategoryModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<BookModel> getBooks() {
		return this.books;
	}

	public void setBooks(List<BookModel> books) {
		this.books = books;
	}

}