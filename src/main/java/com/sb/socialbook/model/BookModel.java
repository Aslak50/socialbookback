package com.sb.socialbook.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import lombok.ToString;


/**
 * The persistent class for the book database table.
 *
 */
@Entity
@ToString
@Table(name="book")
@NamedQuery(name="BookModel.findAll", query="SELECT b FROM BookModel b")
public class BookModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer isbn;

	private String description;

	private String language;

	private String name;

	@Column(name="picture_id_front_cover")
	private String pictureIdFrontCover;

	//bi-directional many-to-many association to CategoryModel
	@ManyToMany(mappedBy="books")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<CategoryModel> categories = new ArrayList<>();

	//bi-directional many-to-many association to ReaderModel
	@ManyToMany(mappedBy="books")
	@ToString.Exclude
	private List<ReaderModel> readers= new ArrayList<>();

	//bi-directional many-to-one association to ProposalModel
	@OneToMany(mappedBy="book1")
	@ToString.Exclude
	private List<ProposalModel> proposals1= new ArrayList<>();

	//bi-directional many-to-one association to ProposalModel
	@OneToMany(mappedBy="book2")
	@ToString.Exclude
	private List<ProposalModel> proposals2= new ArrayList<>();

	//bi-directional many-to-many association to AuthorModel
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name="writes"
			, joinColumns={
					@JoinColumn(name="book_id")
			}
			, inverseJoinColumns={
					@JoinColumn(name="author_id")
			}
			)
	private List<AuthorModel> authors = new ArrayList<AuthorModel>();

	public BookModel() {
	}

	public int getIsbn() {
		return this.isbn;
	}

	public void setIsbn(Integer isbn) {
		this.isbn = isbn;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPictureIdFrontCover() {
		return this.pictureIdFrontCover;
	}

	public void setPictureIdFrontCover(String pictureIdFrontCover) {
		this.pictureIdFrontCover = pictureIdFrontCover;
	}

	public List<CategoryModel> getCategories() {
		return this.categories;
	}

	public void setCategories(List<CategoryModel> categories) {
		this.categories = categories;
	}

	public List<ReaderModel> getReaders() {
		return this.readers;
	}

	public void setReaders(List<ReaderModel> readers) {
		this.readers = readers;
	}

	public List<ProposalModel> getProposals1() {
		return this.proposals1;
	}

	public void setProposals1(List<ProposalModel> proposals1) {
		this.proposals1 = proposals1;
	}

	public ProposalModel addProposals1(ProposalModel proposals1) {
		this.getProposals1().add(proposals1);
		proposals1.setBook1(this);

		return proposals1;
	}

	public ProposalModel removeProposals1(ProposalModel proposals1) {
		this.getProposals1().remove(proposals1);
		proposals1.setBook1(null);

		return proposals1;
	}

	public List<ProposalModel> getProposals2() {
		return this.proposals2;
	}

	public void setProposals2(List<ProposalModel> proposals2) {
		this.proposals2 = proposals2;
	}

	public ProposalModel addProposals2(ProposalModel proposals2) {
		this.getProposals2().add(proposals2);
		proposals2.setBook2(this);

		return proposals2;
	}

	public ProposalModel removeProposals2(ProposalModel proposals2) {
		this.getProposals2().remove(proposals2);
		proposals2.setBook2(null);

		return proposals2;
	}

	public List<AuthorModel> getAuthors() {
		return this.authors;
	}

	public void setAuthors(List<AuthorModel> authors) {
		this.authors = authors;
	}

}