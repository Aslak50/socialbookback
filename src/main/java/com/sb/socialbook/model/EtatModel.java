package com.sb.socialbook.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the etat database table.
 * 
 */
@Entity
@Table(name="etat")
@NamedQuery(name="EtatModel.findAll", query="SELECT e FROM EtatModel e")
public class EtatModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String description;

	private String label;

	//bi-directional many-to-one association to ProposalModel
	@OneToMany(mappedBy="etat")
	private List<ProposalModel> proposals;

	public EtatModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<ProposalModel> getProposals() {
		return this.proposals;
	}

	public void setProposals(List<ProposalModel> proposals) {
		this.proposals = proposals;
	}

	public ProposalModel addProposal(ProposalModel proposal) {
		getProposals().add(proposal);
		proposal.setEtat(this);

		return proposal;
	}

	public ProposalModel removeProposal(ProposalModel proposal) {
		getProposals().remove(proposal);
		proposal.setEtat(null);

		return proposal;
	}

}