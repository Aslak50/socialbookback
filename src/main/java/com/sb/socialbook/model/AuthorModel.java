package com.sb.socialbook.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.ToString;


/**
 * The persistent class for the author database table.
 *
 */
@Entity
@ToString
@Table(name="author")
@NamedQuery(name="AuthorModel.findAll", query="SELECT a FROM AuthorModel a")
public class AuthorModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String firstname;

	private String name;

	//bi-directional many-to-many association to BookModel
	@ManyToMany(mappedBy="authors")
	@ToString.Exclude
	private List<BookModel> books = new ArrayList<BookModel>();

	public AuthorModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<BookModel> getBooks() {
		return this.books;
	}

	public void setBooks(List<BookModel> books) {
		this.books = books;
	}

}