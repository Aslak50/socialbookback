package com.sb.socialbook.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * La classe modèle du reader correspondant à la table reader en BDD.
 * Elle a été générée en utilisant le standard JPA
 * ReaderModel contient les informations d'un lecteur = utilisateur de l'application.
 *
 */
@Entity
@Table(name="reader")
@NamedQuery(name="ReaderModel.findAll", query="SELECT r FROM ReaderModel r")
public class ReaderModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String firstname;

	private String lastname;

	private String location;

	private String mail;

	private String password;

	@Column(name="phone_number")
	private String phoneNumber;

	//bi-directional many-to-one association to MessageModel
	@OneToMany(mappedBy="reader1")
	private List<MessageModel> messages1;

	//bi-directional many-to-one association to MessageModel
	@OneToMany(mappedBy="reader2")
	private List<MessageModel> messages2;

	//bi-directional many-to-many association to BookModel
	@ManyToMany
	@JoinTable(
			name="own"
			, joinColumns={
					@JoinColumn(name="reader_id")
			}
			, inverseJoinColumns={
					@JoinColumn(name="isbn")
			}
			)
	private List<BookModel> books;

	//bi-directional many-to-one association to ProposalModel
	@OneToMany(mappedBy="reader1")
	private List<ProposalModel> proposals1;

	//bi-directional many-to-one association to ProposalModel
	@OneToMany(mappedBy="reader2")
	private List<ProposalModel> proposals2;

	public ReaderModel() {
	}

	public ReaderModel(String firstname, String lastname, String location, String mail, String password,
			String phoneNumber) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.location = location;
		this.mail = mail;
		this.password = password;
		this.phoneNumber = phoneNumber;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<MessageModel> getMessages1() {
		return this.messages1;
	}

	public void setMessages1(List<MessageModel> messages1) {
		this.messages1 = messages1;
	}

	public MessageModel addMessages1(MessageModel messages1) {
		this.getMessages1().add(messages1);
		messages1.setReader1(this);

		return messages1;
	}

	public MessageModel removeMessages1(MessageModel messages1) {
		this.getMessages1().remove(messages1);
		messages1.setReader1(null);

		return messages1;
	}

	public List<MessageModel> getMessages2() {
		return this.messages2;
	}

	public void setMessages2(List<MessageModel> messages2) {
		this.messages2 = messages2;
	}

	public MessageModel addMessages2(MessageModel messages2) {
		this.getMessages2().add(messages2);
		messages2.setReader2(this);

		return messages2;
	}

	public MessageModel removeMessages2(MessageModel messages2) {
		this.getMessages2().remove(messages2);
		messages2.setReader2(null);

		return messages2;
	}

	public List<BookModel> getBooks() {
		return this.books;
	}

	public void setBooks(List<BookModel> books) {
		this.books = books;
	}

	public List<ProposalModel> getProposals1() {
		return this.proposals1;
	}

	public void setProposals1(List<ProposalModel> proposals1) {
		this.proposals1 = proposals1;
	}

	public ProposalModel addProposals1(ProposalModel proposals1) {
		this.getProposals1().add(proposals1);
		proposals1.setReader1(this);

		return proposals1;
	}

	public ProposalModel removeProposals1(ProposalModel proposals1) {
		this.getProposals1().remove(proposals1);
		proposals1.setReader1(null);

		return proposals1;
	}

	public List<ProposalModel> getProposals2() {
		return this.proposals2;
	}

	public void setProposals2(List<ProposalModel> proposals2) {
		this.proposals2 = proposals2;
	}

	public ProposalModel addProposals2(ProposalModel proposals2) {
		this.getProposals2().add(proposals2);
		proposals2.setReader2(this);

		return proposals2;
	}

	public ProposalModel removeProposals2(ProposalModel proposals2) {
		this.getProposals2().remove(proposals2);
		proposals2.setReader2(null);

		return proposals2;
	}

}