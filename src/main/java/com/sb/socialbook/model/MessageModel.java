package com.sb.socialbook.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the message database table.
 * 
 */
@Entity
@Table(name="message")
@NamedQuery(name="MessageModel.findAll", query="SELECT m FROM MessageModel m")
public class MessageModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String content;

	@Column(name="create_at")
	private Timestamp createAt;

	//bi-directional many-to-one association to ProposalModel
	@ManyToOne
	private ProposalModel proposal;

	//bi-directional many-to-one association to ReaderModel
	@ManyToOne
	@JoinColumn(name="author_id")
	private ReaderModel reader1;

	//bi-directional many-to-one association to ReaderModel
	@ManyToOne
	@JoinColumn(name="reader_id1")
	private ReaderModel reader2;

	public MessageModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getCreateAt() {
		return this.createAt;
	}

	public void setCreateAt(Timestamp createAt) {
		this.createAt = createAt;
	}

	public ProposalModel getProposal() {
		return this.proposal;
	}

	public void setProposal(ProposalModel proposal) {
		this.proposal = proposal;
	}

	public ReaderModel getReader1() {
		return this.reader1;
	}

	public void setReader1(ReaderModel reader1) {
		this.reader1 = reader1;
	}

	public ReaderModel getReader2() {
		return this.reader2;
	}

	public void setReader2(ReaderModel reader2) {
		this.reader2 = reader2;
	}

}