package com.sb.socialbook.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sb.socialbook.model.AuthorModel;
import com.sb.socialbook.model.BookModel;

import lombok.NoArgsConstructor;

@Repository
@Transactional
@NoArgsConstructor
public class BookRepository {

	@PersistenceContext
	private EntityManager em;

	public int save(BookModel bookModel, AuthorModel authorModel) {
		this.em.persist(bookModel);
		this.em.persist(authorModel);

		Integer bookId = bookModel.getIsbn();
		Integer authorId = authorModel.getId();

		this.em.createNativeQuery("INSERT INTO social_book2.write VALUES (?,?);")
		.setParameter(1, bookId)
		.setParameter(2, authorId)
		.executeUpdate();

		System.out.println(bookModel.getAuthors());

		return bookModel.getIsbn();
	}

}