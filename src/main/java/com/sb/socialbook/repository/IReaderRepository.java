package com.sb.socialbook.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.sb.socialbook.model.ReaderModel;

@Repository
public interface IReaderRepository extends PagingAndSortingRepository<ReaderModel, Integer>{

}
