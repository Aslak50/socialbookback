package com.sb.socialbook.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.sb.socialbook.model.AuthorModel;

public interface IAuthorRepository extends PagingAndSortingRepository<AuthorModel, Integer> {


}
