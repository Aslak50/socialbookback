package com.sb.socialbook.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.sb.socialbook.model.CategoryModel;

@Repository
public interface ICategoryRepository extends PagingAndSortingRepository<CategoryModel, Integer> {

}
