package com.sb.socialbook.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sb.socialbook.model.AuthorModel;

import lombok.NoArgsConstructor;

/**
 * Contient les requêtes concernant les auteurs.
 */
@Repository
@Transactional
@NoArgsConstructor
public class AuthorRepository {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Ajouter un auteur en BDD
	 */
	public AuthorModel save(AuthorModel authorModel) {
		this.em.persist(authorModel);

		int authorId = authorModel.getId();

		this.em.createNativeQuery("INSERT INTO write (author_id) VALUES (?)").setParameter(1, authorId).executeUpdate();

		return authorModel;
	}
}