package com.sb.socialbook.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.sb.socialbook.model.BookModel;
@Repository
public interface IBookRepository extends PagingAndSortingRepository<BookModel, Integer> {

	public List<BookModel> findByauthorsName(String authorName);
	public List<BookModel> findBycategoriesName (String categoryName);
}
