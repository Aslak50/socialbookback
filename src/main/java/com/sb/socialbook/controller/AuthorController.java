package com.sb.socialbook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sb.socialbook.service.dto.AuthorDTO;
import com.sb.socialbook.service.impl.AuthorService;

/**
 *
 * Permet de traiter les informations de l'auteur d'un livre venant du front.
 *
 */
@RestController
@RequestMapping("/authors")
public class AuthorController {

	private AuthorService authorService;

	@Autowired
	public AuthorController(AuthorService authorService) {
		this.authorService = authorService;
	}

	/**
	 * Reçoit les informations d'un auteur et les envoie au service.
	 *
	 * @param authorDTO
	 * @return AuthorDTO
	 */
	@CrossOrigin(origins = "http://localhost:8100")
	@PostMapping
	public AuthorDTO recupererAuteur(@RequestBody AuthorDTO authorDTO) {
		this.authorService.addAuthors(authorDTO);
		return authorDTO;
	}
}
