package com.sb.socialbook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sb.socialbook.service.dto.BookDTO;
import com.sb.socialbook.service.impl.BookService;

@RestController
@RequestMapping("/books")
public class  BookController{

	private BookService bookService;

	@Autowired
	public BookController(BookService bookService) {
		this.bookService = bookService;
	}

	@CrossOrigin(origins = "http://localhost:8100")
	@PostMapping
	public BookDTO recupererBook(@RequestBody BookDTO bookDTO) {
		this.bookService.saveTest(bookDTO);
		return bookDTO;
	}
}
