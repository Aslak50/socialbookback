package com.sb.socialbook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sb.socialbook.service.dto.ReaderDTO;
import com.sb.socialbook.service.impl.ReaderService;

/**
 *
 * Permet de traiter les informations d'un reader venant du front.
 *
 */
@RestController
@RequestMapping("/readers")
public class ReaderController {

	private ReaderService readerService;

	@Autowired
	public ReaderController(ReaderService readerService) {
		this.readerService = readerService;
	}

	/**
	 * Reçoit les informations d'un reader et les envoie au service.
	 *
	 * @param readerDTO
	 * @return ReaderDTO
	 */
	@CrossOrigin(origins = "http://localhost:8100")
	@PostMapping
	public ReaderDTO recupererReader(@RequestBody ReaderDTO readerDTO) {

		// appalle la méthode registerReader du readerService
		this.readerService.registerReader(readerDTO);

		return readerDTO;
	}
}
